let x = 1397;
let y = 7831;

const sumXY = x + y;
console.log("Result of addition" + sumXY);

const diffXY = x - y;
console.log("Result of subtracion" + diffXY);

const prodXY = x * y;
console.log("Result of multiplication " + prodXY);

const quoXY = x / y;
console.log("Result of division " + quoXY);

const modXY = y % x;
console.log("Result of modulo " + modXY);
console.log("Result of modulo " + (x % y));

let assignmentNumber = 8;
console.log(assignmentNumber);
// ADDTION ASSIGNMENT OPERATOR (+=)
console.log((assignmentNumber += 2));

// SUBTRACTION ASSIGNMENT OPERATOR (-=)
console.log((assignmentNumber -= 2));

// MULTIPLICATION ASSIGNMENT OPERATOR (*=)
console.log((assignmentNumber *= 4));

// DIVISION ASSIGNMENT OPERATOR (/=)
console.log((assignmentNumber /= 8));

// MDAS and PEMDAS
console.log(1 + 2 - (3 * 4) / 5); // Nag o-auto parenthesis pag nag foformat. haha
console.log(1 + (2 - 3) * (4 / 5));

// Pre/Post Incre/Decre

let a = 1;
let b = ++a;
console.log(a, b);
let c = 1;
let d = c++;
console.log(c, d);

let e = 1;
let f = --e;
console.log(e, f);
let g = 1;
let h = g--;
console.log(g, h);

// Coercion
let num1 = 2;
let num2 = "3";
console.log(num1 + num2);
// Non-Coercion
let num3 = 2;
let num4 = 3;
console.log(num3 + num4);

// Remember that true = 1, and false = 0? It just becomes a number that can be used with numbers.
console.log(true + 1);
console.log(false + 1);

// --------------------- TERMS TO REMEMBER -------------------
// Equality operator (==)
// Strict Equality operator (===)
// Inequality operator (!=)
// Strict Inequality operator (!==)
// Relational oprator (< and >)
// ADDTION ASSIGNMENT OPERATOR (+=)
// SUBTRACTION ASSIGNMENT OPERATOR (-=)
// MULTIPLICATION ASSIGNMENT OPERATOR (*=)
// DIVISION ASSIGNMENT OPERATOR (/=)

console.log("one" == "one");
console.log("one" === "one");
